import numpy as np
from datetime import datetime

filename_npz = './trainx16x32_7.npz'
filename_csv = './trainx16x32_7.csv'

print(str(datetime.now()), f'loading {filename_npz} ..')
content = np.load(filename_npz)['arr_0'].astype(np.uintc)
print(str(datetime.now()), f'saving {filename_csv} ..')
np.savetxt(filename_csv, content, fmt='%u', delimiter="\t")
print(str(datetime.now()), 'done')

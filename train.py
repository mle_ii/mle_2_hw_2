import os
import shutil
import traceback
import configparser

from pyspark.ml.feature import HashingTF, IDF
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg.distributed import MatrixEntry, CoordinateMatrix

from sparkc import Spark

from logger import Logger

SHOW_LOG = True

class Trainer():

    def __init__(self) -> None:
        """
        default initialization
        """

        self.config = configparser.ConfigParser()
        self.log = Logger(SHOW_LOG).get_logger(__name__)
        self.config_path = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(self.config_path)

        self.log.info("Trainer ready")
        pass

    def _remove_stored(self, path) -> bool:
        """
        Removing previous execution results
        """
        flag = True
        if os.path.exists(path):
            if os.path.isdir(path):
                shutil.rmtree(path)
            else:
                os.remove(path)
        else:
            self.log.info("No previous runs")
            flag = False
        #check successfulness of operation
        if os.path.exists(path):
            self.log.error(f'Can\'t remove {path}')
            return False
        if flag:
            self.log.info("Previous executions cleared")
        return True

    def _calc_watched_matrix(self, grouped, path='./models/WATCHED') -> bool:
        """
        Matrix of watched movies calculation
        """

        if not self._remove_stored(path):
            return False

        # making right typisation
        matrix = CoordinateMatrix(grouped.flatMapValues(lambda x: x).map(lambda x: MatrixEntry(x[0], x[1], 1.0)))

        try:
            matrix.entries.toDF().write.parquet(path)
            self.config["MODEL"]["WATCHED_PATH"] = path
            self.log.info(f"Matrix saved in: {path}")
        except:
            self.log.error(traceback.format_exc())
            return False
        return os.path.exists(path)

    def _train_tf(self, grouped, path='./models/TF_MODEL'):
        """
        Making TF model
        """

        if not self._remove_stored(path):
            return None

        # Using pd just to some convinient intermediate steps
        df = grouped.toDF(schema=["user_id", "movie_ids"])


        # TF calculation, using 10000 features
        hashingTF = HashingTF(inputCol="movie_ids", outputCol="rawFeatures", numFeatures=10000)
        tf_features = hashingTF.transform(df)

        try:
            hashingTF.write().overwrite().save(path)
            self.config["MODEL"]["TF_PATH"] = path
            self.log.info(f"TF model stored at {path}")
            self.log.info("Features num is 10000")
        except:
            self.log.error(traceback.format_exc())
            return None

        return tf_features

    def _save_idf_features(self, idf_features, path='./models/IDF_FEATURES') -> bool:

        if not self._remove_stored(path):
            return False

        try:
            idf_features.write.format("parquet").save(path, mode='overwrite')
            self.config["MODEL"]["IDF_FEATURES_PATH"] = path
            self.log.info(f"IDF features stored at {path}")
        except:
            self.log.error(traceback.format_exc())
            return False
        return True


    def _train_idf(self, tf_features, path='./models/IDF_MODEL') -> bool:
        """
        IDF model training
        """

        if not self._remove_stored(path):
            return False

        # Calculation of IDF
        idf = IDF(inputCol="rawFeatures", outputCol="features")
        idf = idf.fit(tf_features)

        self.log.info(f"IDF model type: {type(idf)}")

        try:
            idf.write().overwrite().save(path)
            self.config["MODEL"]["IDF_PATH"] = path
            self.log.info(f"IDF model stored at {path}")
        except:
            self.log.error(traceback.format_exc())
            return False

        # For existing users
        idf_features = idf.transform(tf_features)
        if not self._save_idf_features(idf_features):
            return False

        return True


    def train_models(self) -> bool:

        try:
            sparkc = Spark()
            sc = sparkc.get_context()
            spark = sparkc.get_session()
        except:
            self.log.error(traceback.format_exc())
            return False

        try:
            INPUT_FILENAME = self.config.get("DATA", "INPUT_FILE")
        except:
            self.log.error("No data on provided in config path")

        self.log.info(f'train data filename = {INPUT_FILENAME}')

        # Convinient groupping data by user_id
        grouped = sc.textFile(INPUT_FILENAME, sparkc.num_parts) \
            .map(lambda x: map(int, x.split())).groupByKey() \
            .map(lambda x : (x[0], list(x[1])))

        # Martix calculation
        self.log.info('Calculating matrix of watched movies')
        if not self._calc_watched_matrix(grouped):
            self.log.error("Error calculation matrix of watched movies")
            return False

        # TF model fit
        self.log.info('Train TF model')
        tf_features = self._train_tf(grouped)
        if tf_features is None:
            self.log.error("Error training TF model")
            return False

        # IDF model fit
        self.log.info('Train IDF model')
        if not self._train_idf(tf_features):
            self.log.error("Error training IDF model")
            return False

        #updating conf
        os.remove(self.config_path)
        with open(self.config_path, 'w') as configfile:
            self.config.write(configfile)

        return True

if __name__ == "__main__":
    trainer = Trainer()
    trainer.train_models()

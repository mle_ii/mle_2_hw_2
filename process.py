import os
import shutil
import traceback
import configparser

from pyspark.ml.feature import HashingTF, IDF, IDFModel
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg.distributed import MatrixEntry, CoordinateMatrix

# just for generating test data
import numpy as np

from sparkc import Spark

from logger import Logger

SHOW_LOG = True


def test_smp_and_rand():
    tst = Processor()
    assert True == tst.random()
    assert True == tst.sample()



class Processor():

    def __init__(self):
        """
        default initialization
        """

        self.config = configparser.ConfigParser()
        self.log = Logger(SHOW_LOG).get_logger(__name__)
        self.config_path = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(self.config_path)

        # spark contest and session
        try:
            self.sparkc = Spark()
            self.sc = self.sparkc.get_context()
            self.spark = self.sparkc.get_session()
        except:
            self.log.error(traceback.format_exc())

        # loading models
        if not self._load_models():
            self.log.error("Can not load models")
            raise Exception("Can not load models")
        
        self.log.info("processor is ready")
        pass

    def _load_watched(self) -> bool:
        """
        Movie matrix loading
        """

        path = self.config.get("MODEL", "WATCHED_PATH")
        if path is None or not os.path.exists(path):
            self.log.error('Matrix of watched movies doesn\'t exists')
            return False
        
        self.log.info(f'Reading {path}')
        try:
            self.watched = CoordinateMatrix(self.spark.read.parquet(path) \
                .rdd.map(lambda row: MatrixEntry(*row)))
        except:
            self.log.error(traceback.format_exc())
            return False
        return True
    
    def _load_tf(self) -> bool:
        """
        loading TF model
        """

        path = self.config.get("MODEL", "TF_PATH")
        if path is None or not os.path.exists(path):
            self.log.error('TF model does not exists')
            return False
        
        self.log.info(f'Reading {path}')
        try:
            self.hashingTF = HashingTF.load(path)
        except:
            self.log.error(traceback.format_exc())
            return False
        return True
    
    def _load_idf(self) -> bool:
        """
        loading IDF model
        """

        path = self.config.get("MODEL", "IDF_PATH")
        if path is None or not os.path.exists(path):
            self.log.error('IDF model doesn\'t exists')
            return False
        
        self.log.info(f'Reading {path}')
        try:
            self.idf = IDFModel.load(path)
        except:
            self.log.error(traceback.format_exc())
            return False
        return True
    
    def _load_idf_features(self) -> bool:
        """
        IDF Features loading
        """
        
        path = self.config.get("MODEL", "IDF_FEATURES_PATH")
        if path is None or not os.path.exists(path):
            self.log.error('IDF features doesn\'t exists')
            return False
        
        self.log.info(f'Reading {path}')
        try:
            self.idf_features = self.spark.read.load(path)
        except:
            self.log.error(traceback.format_exc())
            return False
        return True

    def _load_models(self) -> bool:
        """
        All mentioned above loading
        """

        self.log.info('Loading Matrix of watched movies')
        self._load_watched()

        self.log.info('Loading TF model')
        self._load_tf()

        self.log.info('Loading IDF model')
        self._load_idf()

        self.log.info('Loading IDF features')
        self._load_idf_features()

        return True
    
    def _get_recomendation(self, ordered_similarity, max_count=5) -> list:
        """
        Recomendadtion calculation
        input: list of IndexedRow
        output: (movie_id, rank)
        """
        
        self.log.info('Calculate movies ranks')

        #using user-similarity as weight for his films
        #and counting film rang
        users_sim_matrix = IndexedRowMatrix(ordered_similarity)
        
        # film rang multyplyed on weights
        multpl = users_sim_matrix.toBlockMatrix().transpose().multiply(self.watched.toBlockMatrix())
        
        ranked_movies = multpl.transpose().toIndexedRowMatrix().rows.sortBy(lambda row: row.vector.values[0], ascending=False)

        result = []
        for i, row in enumerate(ranked_movies.collect()):
            if i >= max_count:
                break
            result.append((row.index, row.vector[0]))
        return result

    def sample(self):
        """
        recomendations for user (from test dataset)
        """

        self.log.info('Sample existing user recomendation')

        # gettin user martix of similarites
        temp_matrix = IndexedRowMatrix(self.idf_features.rdd.map(lambda row: IndexedRow(row["user_id"], Vectors.dense(row["features"]))))
        temp_block = temp_matrix.toBlockMatrix()

        self.log.info('Calculate similarities')

        # calculationg consine similarities
        similarities = temp_block.transpose().toIndexedRowMatrix().columnSimilarities()

        #chosing one user
        user_id = np.random.randint(low=0, high=self.watched.numCols())
        self.log.info(f'Random user ID: {user_id}')

        # from our matrix of similarity getting mods look a like users by filtering
        filtered = similarities.entries.filter(lambda x: x.i == user_id or x.j == user_id)

        # getting most similar users
        ordered_similarity = filtered.sortBy(lambda x: x.value, ascending=False) \
            .map(lambda x: IndexedRow(x.j if x.i == user_id else x.i, Vectors.dense(x.value)))

        recomendations = self._get_recomendation(ordered_similarity)
        self.log.info('TOP recomendations for existing user:')
        for movie_id, rank in recomendations:
            self.log.info(f'- movie # {movie_id} (rank: {rank})')

        return True

    def random(self):
        """
        Recomendations by films
        """

        self.log.info('Sample new user')

        watched_movies = np.random.randint(low=0, high=self.watched.numCols(), size=int(self.watched.numCols()/4)).tolist()
        self.log.info(f"Movies list:{watched_movies}")
        newdf = self.sc.parallelize([[-1, watched_movies]]).toDF(schema=["user_id", "movie_ids"])
        new_tf_features = self.hashingTF.transform(newdf)
        new_idf_features = self.idf.transform(new_tf_features)
        new_idf_features = new_idf_features.first()["features"]

        self.log.info('Calculate similarities')

        #Calc. similaritys with someone who already in dataset
        # cosine distace
        similarities = self.idf_features.rdd.map(
            lambda row: IndexedRow(
                row["user_id"],
                Vectors.dense(new_idf_features.dot(row["features"] / (new_idf_features.norm(2) * row["features"].norm(2))))
            )
        )
        ordered_similarity = similarities.sortBy(lambda x: x.vector.values[0], ascending=False)
        recomendations = self._get_recomendation(ordered_similarity)
        self.log.info('TOP recomendations by films:')
        for movie_id, rank in recomendations:
            self.log.info(f'- movie # {movie_id} (rank: {rank})')
        return True

if __name__ == "__main__":
    processor = Processor()
    processor.sample()
    processor.random()
